# Simple Pipeline for a Flask application Deployed to EKS cluster

# Summary

This pipeline has different stages and below is the summary of what is done at each stage.

**Static Analysis:**
Flake8 python library is used to check the code style used in this Code. This reports for any errors or code style in the repo.

**Unit test:**
A dummy unit test is written for the sake of simplicity to check for the unit test to pass.

**vulnerablity scan:**
Trivy(Opensource vulnerablity scanner) is used in this code to check for any vulnerablities in the source code.It produces report on the binaries used in the source code.

**Build:**
Docker images are build in the stage and pushed to the gitlab image container registry

**Deploy:**
The build images are deployed into the EKS cluster using the dynamically created tag which is passed through helm values.yaml file.
